#!/bin/sh
if [ ! -d "target" ]; then
  mkdir "target"
fi
if cd target || exit; then
  clone=false
  if [ ! -d "dndbot" ]; then
    git clone git@bitbucket.org:alexanderhems/dndbot.git
    clone=true
  fi
  if cd dndbot || exit; then
    if [ ! $clone ]; then
      git pull
    fi
    mvn package
    if cd ../../ || exit; then
      if [ ! -d "bot-jars" ]; then
        mkdir "bot-jars"
      fi
      cp target/dndbot/target/dndbot-1.0-SNAPSHOT-jar-with-dependencies.jar bot-jars/dndbot.jar
    fi
  else
    echo "cd dndbot failed"
  fi
else
  echo "cd target failed"
fi
