const express = require('express');
server = function(scribejs,port,logger){
  const app = express();
  app.use('/watchdog', scribejs.webPanel());
  app.listen(port, function () {
    logger.logInit('Watchdog Logging is active on Path /watchdog')
  })
}
module.exports = server;
