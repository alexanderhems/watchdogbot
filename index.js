const scribe = require('scribe-js')();
const console = process.console;
const logger = require('./logging/logger')(console);
const server = require('./express/server')(scribe,3456,logger);
const client = require('./discord/client')(logger);
