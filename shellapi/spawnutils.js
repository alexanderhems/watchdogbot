const spawn = require('child_process').spawn;

spawnutils = function(logger){
  return {
    customSpawn : function(command, options) {
      var spawnObject = spawn(command,options, {detached: true});
      this.spawnLog(spawnObject)
      return spawnObject;
    },
    spawnLog : function(spawnObject){
      spawnObject.stdout.on('data', function(data){
        logger.logStdOut(data.toString());
      });
      spawnObject.stderr.on('data', function(data){
        logger.logStdErr(data.toString());
      });
      spawnObject.on('exit', function (code) {
        logger.logERROR('child process exited with code ' + code);
      });
    }
  }
}

module.exports = spawnutils;
