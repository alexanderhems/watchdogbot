const kill = require('tree-kill')
shellutils = function(logger){
  const spawnUtils = require('./spawnutils')(logger);
  return {

    killDndbot : function(bot){
        kill(bot.pid);
        return true;
    },

    startDndbot : function(){
        return spawnUtils.customSpawn('sh',['scripts/dndbot.sh']);
    },

    updateDNDBOT : function(){
        return spawnUtils.customSpawn('sh', ['scripts/updatedndbot.sh'])
    }


  }
}
module.exports = shellutils;
