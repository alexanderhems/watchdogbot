function logger(console){
  return {
    logInit: function(message){
      this.logCustomTag("INIT",message);
    },
    logStdOut: function(message){
      this.logCustomTag("STDOUT",message);
    },
    logStdErr: function(message){
      this.logCustomTag("STDERR",message);
    },
    logERROR: function(message){
      this.logCustomTag("ERROR",message);
    },
    logCommand: function(message){
      this.logCustomTag("COMMAND",message);
    },
    logCustomTag: function(tag,message){
      console.tag(tag).log(message);
    }
  }
}
module.exports = logger;
